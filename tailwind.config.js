module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: [],
  theme: {
    extend: {},
    container: {
      center: true,
    },
    screens: {
      xs: '500px',
      sm: '640px',
      md: '768px',
      lg: '992px',
      xl: '1366px',
    },
  },
  variants: {},
  plugins: [],
};
