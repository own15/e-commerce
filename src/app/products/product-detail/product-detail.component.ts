import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Product} from '../../shared/interfaces/product';
import {CartService} from '../../services/cart.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {
  public productDetail: Product;
  public config = {
    direction: 'horizontal',
    slidesPerView: 1,
    keyboard: true,
    autoplay: true,
    spaceBetween: 10,
    loop: true,
    speed: 1000,
    pagination: true
  };

  constructor(
    private route: ActivatedRoute,
    private cartService: CartService) {
  }

  ngOnInit(): void {
    this.productDetail = JSON.parse((this.route.snapshot.data.productDetail));
  }

  addToCart(product) {
    const item = {
      title: product.title,
      price: product.price,
      qty: 1
    };

    this.cartService.addToCart(item);
  }

}
