import {Injectable} from '@angular/core';
import {catchError} from 'rxjs/operators';
import {of} from 'rxjs';
import {ProductsService} from '../../services/products.service';

@Injectable({
  providedIn: 'root'
})
export class CategoriesResolve {

  constructor(private productsService: ProductsService) { }

  resolve() {
    return this.productsService.getCategories().pipe(
      catchError(err => {
        return of(err);
      })
    );
  }

}
