import {Injectable} from '@angular/core';
import {catchError} from 'rxjs/operators';
import {of} from 'rxjs';
import {ProductsService} from '../../services/products.service';
import {ActivatedRouteSnapshot} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ProductDetailResolve {

  constructor(private productsService: ProductsService) { }

  resolve(route: ActivatedRouteSnapshot) {
    return this.productsService.getProductDetail(route.params.slug).pipe(
      catchError(err => {
        return of(err);
      })
    );
  }

}
