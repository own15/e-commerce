import { Component, OnInit } from '@angular/core';
import {CartService} from '../services/cart.service';

interface Item {
  title: string;
  price: string;
  qty: number;
}

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  cartItems: Array<Item>;
  summaryPrice = 0;

  constructor(
    private cartService: CartService
  ) { }

  ngOnInit(): void {
    this.cartItems = this.cartService.getItems();

    this.cartItems.map((el: Item) => {
      this.summaryPrice += parseInt(el.price, 0) * el.qty;
    });
  }

  remove(product) {
    this.cartService.removeItem(product);
    this.cartItems = this.cartService.getItems();
  }
}
