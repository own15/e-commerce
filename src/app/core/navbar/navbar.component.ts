import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CartService} from '../../services/cart.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  openNav;
  cartQty;

  constructor(
    public router: Router,
    private cartService: CartService) {
  }

  ngOnInit(): void {
    if (localStorage.getItem('cart_qty')) {
      this.cartQty = localStorage.getItem('cart_qty');
    }

    this.cartService.cartQty.subscribe(qty => this.cartQty = qty);
    this.cartService.getItems();
  }

}
