export interface Product {
  image: string;
  background: Array<string>;
  title: string;
  price: string;
  slug: string;
}
