import {Product} from './product';

export interface ProductsList {
  products: {[key: string]: Product};
}
