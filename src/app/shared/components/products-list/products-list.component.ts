import {Component, Input, OnInit} from '@angular/core';

interface Item {
  image: string;
  title: string;
  price: number;
  slug: string;
}

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements OnInit {
  @Input() products: Item[];

  constructor() {
  }

  ngOnInit(): void {
  }

}
