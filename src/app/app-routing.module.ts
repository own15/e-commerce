import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {FavoriteProductsResolve} from './resolvers/products/favorite-products-resolve';
import {CategoriesResolve} from './resolvers/products/categories-resolve';
import {ProductDetailResolve} from './resolvers/products/product-detail-resolve';


const routes: Routes = [
  {
    path: 'account',
    loadChildren: () =>
      import('./account/account.module').then(m => m.AccountModule)
  },
  {
    path: 'cart',
    loadChildren: () =>
      import('./cart/cart.module').then(m => m.CartModule)
  },
  {
    path: '',
    component: HomeComponent,
    resolve: {favoriteProducts: FavoriteProductsResolve, categories: CategoriesResolve},
    children: [
      {
        path: ':slug',
        loadChildren: () =>
          import('./products/product-detail/product-detail.module')
            .then(m => m.ProductDetailModule),
        resolve: {productDetail: ProductDetailResolve}
      }
    ]
  },
  {
    path: '*',
    component: HomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
