import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ProductsList} from '../shared/interfaces/products-list';
import {Categories} from '../shared/interfaces/categories';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  favoriteProducts: ProductsList;
  categories: Categories;
  selectedCategory: string;

  constructor(
    public route: ActivatedRoute) { }

  ngOnInit(): void {
    this.favoriteProducts = JSON.parse((this.route.snapshot.data.favoriteProducts));
    this.categories = JSON.parse((this.route.snapshot.data.categories));
    this.selectedCategory = this.categories.categories[0];
  }

}
