import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  items = [];
  cartQty: BehaviorSubject<number> = new BehaviorSubject<number>(0);

  constructor() {
    this.setValue();
  }

  setValue() {
    if (localStorage.getItem('cart')) {
      this.items = JSON.parse(localStorage.getItem('cart'));
      this.cartQty.next(JSON.parse(localStorage.getItem('cart_qty')));
    }
  }

  addToCart(product) {
    this.cartQty.next(this.cartQty.getValue() + 1);

    // if array have elements -> check this element exist in array
    const index = this.items.findIndex(el => el.title === product.title);
    if (index === -1) {
      this.items.push(product);
    } else {
      this.items[index].qty++;
    }

    // save in localstorage new array
    this.sendCart();
  }

  removeItem(product) {
    this.cartQty.next(this.cartQty.getValue() - 1);
    const index = this.items.findIndex(el => el.title === product.title);
    if (this.items[index].qty > 1) {
      this.items[index].qty--;
    } else {
      this.items.splice(this.items[index], 1);
    }

    // save in localstorage new array
    this.sendCart();
  }

  getItems() {
    return this.items;
  }

  sendCart() {
    const cart = JSON.stringify(this.items);
    const qtyCart = JSON.stringify(this.cartQty.getValue());
    localStorage.setItem('cart', cart);
    localStorage.setItem('cart_qty', qtyCart);
  }
}
