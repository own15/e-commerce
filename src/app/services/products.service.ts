import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private http: HttpClient) {
  }

  getProducts() {
    return this.http.get('/assets/products/favoriteproducts.json', {responseType: 'text'});
  }

  getCategories() {
    return this.http.get('/assets/products/categories.json', {responseType: 'text'});
  }

  getProductDetail(slug: string) {
    return this.http.get(`/assets/products/${slug}.json`, {responseType: 'text'});
  }
}
